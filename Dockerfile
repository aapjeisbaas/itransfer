FROM unit:python
COPY requirements.txt /config/requirements.txt
COPY config.json /docker-entrypoint.d/
COPY itransfer.py /www/main.py
COPY templates /www/templates
COPY static /www/static
COPY dir-create.sh /docker-entrypoint.d/ 
ENV STORAGE_PATH='/www/uploads'
RUN python3 -m pip install -r /config/requirements.txt ; rm -rf /root/.cache