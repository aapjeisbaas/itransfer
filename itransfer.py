import os
import uuid
import json
import datetime
from flask import Flask, request, render_template, send_file

# Get the storage location from the environment variable
storage_location = os.environ.get("STORAGE_PATH", "uploads")

# Set the purge duration (in seconds)
purge_duration = 7 * 24 * 60 * 60  # 1 week

# background thread components for cleanup
import threading

# import atexit # not necessary at the moment

POOL_TIME = 60  # Pool start interval in seconds

# variables that are accessible from anywhere
common_data_struct = {}
# lock to control access to variable
data_lock = threading.Lock()
# timer handler
your_timer = threading.Timer(0, lambda x: None, ())

UPLOAD_TEMP = os.environ.get("TMP_PATH", storage_location)
UPLOAD_LOCK = {}


def chunker(r):
    return {
        "chunkNumber": int(r.get("resumableChunkNumber")),
        "chunkSize": r.get("resumableChunkSize"),
        "totalChunks": int(r.get("resumableTotalChunks")),
        "identifier": r.get("resumableIdentifier"),
        "path": r.get("resumableRelativePath"),
        "fileName": r.get("resumableFilename"),
        "fileSize": r.get("resumableTotalSize"),
        "tempPath": os.path.join(UPLOAD_TEMP, r.get("resumableIdentifier")),
        "tempChunk": os.path.join(
            UPLOAD_TEMP,
            r.get("resumableIdentifier"),
            r.get("resumableChunkNumber") + ".part",
        ),
    }


def create_app():
    """
    Initializes and returns a Flask application.
    Initializes a background thread for cleanup.

    Returns:
        app (Flask): The Flask application.
    """
    app = Flask(__name__)

    def interrupt():
        global your_timer
        your_timer.cancel()

    def do_stuff():
        global common_data_struct
        global your_timer
        with data_lock:
            purge_files()

        # Set the next timeout to happen
        your_timer = threading.Timer(POOL_TIME, do_stuff, ())
        your_timer.start()

    def do_stuff_start():
        # Do initialisation stuff here
        global your_timer
        # Create your timer
        your_timer = threading.Timer(POOL_TIME, do_stuff, ())
        your_timer.start()

    # Initiate
    # do_stuff_start() # not necessary at the moment
    # When you kill Flask (SIGTERM), cancels the timer
    # atexit.register(interrupt) # not necessary at the moment
    return app


app = create_app()


def id_generator(folder):
    """ "Returns an available short random file name inside the folder."""
    while True:
        file_name = str(uuid.uuid4())[:4]
        if not os.path.isfile(f"{folder}/{file_name}"):
            return file_name

# handled by nginx unit
# @app.route("/", methods=["GET"])
# def index():
#     """
#     A function that serves as the endpoint for the home page of the web application.
    
#     Returns:
#         A rendered HTML template for the home page.
#         templates/index.html
#     """
#     return render_template("index.html")


@app.route("/uploads", methods=["GET"])
def checker():
    """
    A function that checks if a file chunk exists based on the provided request arguments.
    This function uses the chunker function defined above, to implement the chunking logic from resumable.js

    Return:
    - If the file exists: an empty string with a status code of 200.
    - If the file does not exist: an empty string with a status code of 404.
    """
    current = chunker(request.args)

    if os.path.isfile(current["tempChunk"]):
        return ("", 200)
    else:
        return ("", 404)


# (D) HANDLE FILE CHUNK UPLOAD
@app.route("/uploads", methods=["POST"])
def uploader():
    # (D1) GET CURRENT CHUNK INFORMATION
    current = chunker(request.args)

    # (D2) CREATE TEMP FOLDER IF NOT ALREADY CREATED
    if not os.path.isdir(current["tempPath"]):
        try:
            # handle multi threaded start of upload
            os.mkdir(current["tempPath"], 0o777)
        except FileExistsError:
            pass

    # (D3) SAVE CHUNK
    request.files["file"].save(current["tempChunk"])

    # (D4) ALL CHUNKS UPLOADED?
    complete = True
    for i in reversed(range(1, current["totalChunks"] + 1)):
        if not os.path.exists(os.path.join(current["tempPath"], str(i) + ".part")):
            complete = False
            break

    # (D5) ON COMPLETION OF ALL CHUNKS
    if complete and not current["identifier"] in UPLOAD_LOCK:
        # (D5-1) LOCK
        UPLOAD_LOCK[current["identifier"]] = 1

        # (D5-2) LET OUR POWERS COMBINE!
        # generate random location
        file_id = id_generator(storage_location)
        file_path = os.path.join(storage_location, file_id)

        with open(file_path, "ab") as fullfile:
            for i in range(1, current["totalChunks"] + 1):
                chunkName = os.path.join(current["tempPath"], str(i) + ".part")
                with open(chunkName, "rb") as chunkFile:
                    fullfile.write(chunkFile.read())
                os.unlink(chunkName)
            fullfile.close()
            os.rmdir(current["tempPath"])
        
        # Get file metadata
        filename = current["fileName"]
        filesize = os.path.getsize(file_path)
        timestamp = datetime.datetime.now().timestamp()

        # Create metadata dictionary
        metadata = {
            "filename": filename,
            "filesize": filesize,
            "timestamp": timestamp,
        }
        # Write metadata to JSON file
        metadata_file_path = f"{file_path}.json"
        with open(metadata_file_path, "w") as f:
            json.dump(metadata, f)

        # (D5-3) UNLOCK
        del UPLOAD_LOCK[current["identifier"]]
        return (file_id, 201)

    return ("", 200)


@app.route("/download/<file_id>", methods=["GET"])
def download(file_id):
    """
    Endpoint for downloading a file.
    The flask send_file function is used to download the file with the original filename.

    Parameters:
        file_id (str): The ID of the file to be downloaded.

    Returns:
        str or File: The downloaded file if it exists, or a 404 error message if the file is not found.
    """
    file_path = os.path.join(storage_location, file_id)

    # Throw error if file doesn't exist
    if not os.path.isfile(file_path):
        return ("File not found.", 404)

    with open(f"{file_path}.json", "r") as file:
        file_metadata = json.load(file)

    return send_file(
        file_path, download_name=file_metadata["filename"], as_attachment=True
    )


@app.route("/received/<file_id>", methods=["GET"])
def received(file_id):
    """
    Retrieves the json metadata from disk and returns the download URL for a received file.

    Args:
        file_id (str): The ID of the file to retrieve.

    Returns:
        str: The rendered HTML template with the download URL and filename.
        
    Raises:
        FileNotFound: 404 If the file does not exist.
    """
    file_path = os.path.join(storage_location, file_id)

    # Throw error if file doesn't exist
    if not os.path.isfile(file_path):
        return ("File not found.", 404)

    with open(f"{file_path}.json", "r") as file:
        file_metadata = json.load(file)

    download_url = request.host_url + "direct-download/" + file_id
    return render_template(
        "received.html", download_url=download_url, filename=file_metadata["filename"]
    )


def purge_files():
    """
    Deletes files in the specified storage location that have not been modified within the given purge duration.
    """
    current_time = datetime.datetime.now().timestamp()

    for filename in os.listdir(storage_location):
        file_path = os.path.join(storage_location, filename)
        file_modified_time = os.path.getmtime(file_path)

        if current_time - file_modified_time >= purge_duration:
            os.remove(file_path)


if __name__ == "__main__":
    app.run(host="0.0.0.0")
