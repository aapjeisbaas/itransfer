# iTransfer

iTransfer is a file transfer application built with Flask and can be deployed on any container platform.

## TLDR; I just want to run in now

```shell
docker run -p 8000:8000 aapjeisbaas/itransfer:latest
```

## Build and deploy

The iTransfer application can be build and deployed in a container using the following steps:

1. Create a directory on your host machine to be mounted as a volume:

```shell
mkdir /path/to/host/volume
```

2. Build the Docker image:

```shell
docker build -t itransfer .
```

3. Run the Docker container, mounting the host volume to the container volume::

```shell
docker run -p 8000:8000 -e STORAGE_PATH=/mnt/itransfer/ -v /path/to/host/volume:/mnt/itransfer/ itransfer
```

> Note: Modify the `-p` flag if you want to use a different port.

## Configuration

The iTransfer application can be configured using environment variables. The following environment variables are available:

- `STORAGE_PATH`: The path where files will be stored. Default: `/mnt/itransfer/`

## K8s deployment

This is just an example, tweak it to what you need.

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: itransfer-deployment
  namespace: default
spec:
  selector:
    matchLabels:
      app: itransfer-container
  strategy: 
     type: RollingUpdate
  template:
    metadata:
      labels:
        app: itransfer-container
    spec:
      containers:
      - name: itransfer
        image: aapjeisbaas/itransfer:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 8000
          name: http
        volumeMounts:
        - name: itransfer-volume
          mountPath: /mnt/itransfer
        env:
        - name: STORAGE_PATH
          value: "/mnt/itransfer/"
        readinessProbe:
          httpGet:
            path: /
            port: http
          initialDelaySeconds: 5
          periodSeconds: 5
      volumes:
      - name: itransfer-volume
        persistentVolumeClaim:
          claimName: itransfer-pvc
          readOnly: false
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: itransfer-pvc
  namespace: default
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 200Gi
---
apiVersion: v1
kind: Service
metadata:
  name: itransfer-service
  namespace: default
spec:
  ports:
  - port: 80
    targetPort: 8000
    name: tcp
  selector:
    app: itransfer-container

```

## Usage

Once the iTransfer application is running, you can access it using the following URL:
> http://localhost:8000
