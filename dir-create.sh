#!/bin/bash

# STORAGE_PATH is used for storing uploaded files, make sure it exists and is writable 
mkdir -p ${STORAGE_PATH}
chown 999:999 ${STORAGE_PATH}

# this link is used by nginx to access the files directly
ln -s ${STORAGE_PATH} /www/direct-download

